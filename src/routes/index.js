// @flow
import React, { PureComponent, Fragment } from "react";
import { connect } from "react-redux";
import { BrowserRouter as Router, Switch } from "react-router-dom";
import createBrowserHistory from "history/createBrowserHistory";

import {
  isAuthenticated,
  isUserInitiated
} from "../containers/auth/store/selectors";
import { authSignOut } from "../containers/auth/store/action";

// import components
import routes from "./routes";
import PrivateRoute from "./Private";
import PublicRoute from "./Public";

import Layout from "../components/Layout";

import { UserSettingsProvider } from "../contexts/UserSettings";

const history = createBrowserHistory();
type RoutesProps = {
  isAuthenticated: Boolean,
  isUserInitiated: Boolean,
  compareList: string[],
  userSettings: Object,
  authSignOut: Function
};

class Routes extends PureComponent<RoutesProps> {
  render() {
    const {
      isAuthenticated,
      isUserInitiated,
      compareList,
      userSettings,
      authSignOut
    } = this.props;
    return (
      <Fragment>
        {isUserInitiated ? (
          // $FlowFixMe
          <Router hisotry={history}>
            <Switch>
              <UserSettingsProvider>
                <Layout
                  isAuthenticated={isAuthenticated}
                  compareList={compareList}
                  userSettings={userSettings}
                  authSignOut={authSignOut}
                >
                  {routes.map((route, i) => {
                    if (route.auth) {
                      return (
                        <PrivateRoute
                          isAuthenticated={isAuthenticated}
                          key={i}
                          {...route}
                        />
                      );
                    }
                    return <PublicRoute key={i} {...route} />;
                  })}
                </Layout>
              </UserSettingsProvider>
            </Switch>
          </Router>
        ) : (
          <Fragment />
        )}
      </Fragment>
    );
  }
}

const Actions = {
  authSignOut
};

function mapStateToProps(state) {
  return {
    isAuthenticated: isAuthenticated(state),
    isUserInitiated: isUserInitiated(state),
    compareList: state.course.compareList,
    userSettings: state.profile.settings
  };
}

export default connect(
  mapStateToProps,
  Actions
)(Routes);
