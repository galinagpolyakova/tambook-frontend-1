import courseRoutes from "../containers/course/routes";
import generalRoutes from "../containers/general/routes";
import bookingRoutes from "../containers/booking/routes";
import profileRoutes from "../containers/profile/routes";
import authRoutes from "../containers/auth/routes";

export default [
  ...courseRoutes,
  ...generalRoutes,
  ...bookingRoutes,
  ...profileRoutes,
  ...authRoutes
];
