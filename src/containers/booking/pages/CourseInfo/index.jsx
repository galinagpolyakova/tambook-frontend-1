// @flow
import { type CourseType, type CourseFeeType } from "../../../course/types";

import React, { PureComponent, Fragment } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";

import Row from "../../../../shared/components/Row";
import Col from "../../../../shared/components/Col";
import Sidebar, { Widget } from "../../../../components/Sidebar";
import PageHeader from "../../../../components/PageHeader";
import Section from "../../../../shared/components/Section";
import Loader from "../../../../components/Loader";
import Currency from "../../../../components/Currency";

import { isEmail } from "../../../../shared/kernel/cast";
import { queryParamsParse } from "../../../../shared/helpers/url";

import { getCourse } from "../../../course/store/actions";
import { bookACourse } from "../../store/actions";
import {
  isCourseLoaded,
  getConsolidatedCourse
} from "../../../course/store/selectors";

import "./styles.css";

import ProgressBar from "../../components/ProgressBar";

import BookingDetailForm from "./BookingDetailForm";
import AccomodationForm from "./AccomodationForm";
import TravelDetailForm from "./TravelDetailForm";
import PersonalInfoForm from "./PersonalInfoForm";
import PaymentSchemeForm from "./PaymentSchemeForm";

type CourseInfoProps = {
  bookACourse: Function,
  match: {
    params: {
      courseId: number
    }
  },
  isLoaded: boolean,
  getCourse: Function,
  course: CourseType,
  startDates: any,
  bookingId: null | string,
  isBookingSuccess: boolean,
  passport: string,
  location: any,
  currencyType: string,
  loading: boolean
};

type CourseInfoState = {
  form: {
    email: string,
    firstName: string,
    lastName: string,
    dob: string,
    phoneNo: string,
    nativeLanguage: string
  },
  formErrors: {
    email: null | string,
    firstName: null | string,
    lastName: null | string,
    dob: null | string,
    phoneNo: null | string,
    nativeLanguage: null | string
  },
  accommodation: {
    type: null | string,
    startDate: null | string,
    endDate: null | string,
    homeStayWeeks: number,
    youngChildren: string,
    olderChildren: string,
    olderAdults: string,
    pets: string,
    smoke: string,
    foodRestrictions: string,
    alergies: string,
    medicalConditions: string
  },
  accommodationForm: {
    checked: string,
    showPreferences: boolean,
    selectOptions: Array<string>
  },
  payment: {
    method: string
  },
  isRangeSelected: boolean,
  selectedWeek: number | null,
  selectedRange: number | null,
  courseStartDate: Date | null,
  step: number
};

class CourseInfo extends PureComponent<CourseInfoProps, CourseInfoState> {
  STEP = 4;
  OPTION1 = "option1";
  OPTION2 = "option2";
  OPTION3 = "option3";
  SELF_ARRANGE = "selfArrange";
  ABOVE18 = "above18";
  UNDER18 = "under18";
  STRIPE = "stripe";

  state = {
    form: {
      email: "",
      firstName: "",
      lastName: "",
      dob: "",
      phoneNo: "",
      nativeLanguage: ""
    },
    formErrors: {
      email: null,
      firstName: null,
      lastName: null,
      dob: null,
      phoneNo: null,
      nativeLanguage: null
    },
    accommodation: {
      type: this.SELF_ARRANGE,
      startDate: null,
      endDate: null,
      homeStayWeeks: 1,
      youngChildren: "NOT SPECIFIED",
      olderChildren: "NOT SPECIFIED",
      olderAdults: "NOT SPECIFIED",
      pets: "NOT SPECIFIED",
      smoke: "NOT SPECIFIED",
      foodRestrictions: "NOT SPECIFIED",
      alergies: "NOT SPECIFIED",
      medicalConditions: "NOT SPECIFIED"
    },
    accommodationForm: {
      checked: this.OPTION1,
      showPreferences: false,
      selectOptions: ["NOT SPECIFIED", "YES", "NO"]
    },
    payment: {
      method: ""
    },
    selectedRange: null,
    isRangeSelected: false,
    selectedWeek: null,
    courseStartDate: null,
    step: 1
  };

  componentDidMount() {
    const {
      match: {
        params: { courseId }
      }
    } = this.props;
    // $FlowFixMe
    const query = queryParamsParse(this.props.location.search);

    if ("selectedWeek" in query) {
      this.setState({
        selectedWeek: query.selectedWeek
      });
    } else {
      this.setState({
        selectedWeek: 0
      });
    }
    if ("selectedRange" in query) {
      this.onRangeSelect(query.selectedRange);
    }
    !this.props.isLoaded ? this.props.getCourse(courseId) : "";
  }

  handleFormFieldChange = event => {
    this.setState({
      form: {
        ...this.state.form,
        [event.target.id]: event.target.value
      }
    });
  };

  validateForm = () => {
    const {
      email,
      firstName,
      nativeLanguage,
      lastName,
      dob,
      phoneNo
    } = this.state.form;

    let hasError = false;

    this.resetFormErrors();

    if (email === "") {
      this.setFormErrors("email", "Email is required.");
      hasError = true;
    } else if (!isEmail(email)) {
      this.setFormErrors("email", "Email is invalid.");
      hasError = true;
    }

    if (firstName === "") {
      this.setFormErrors("firstName", "First Name is required.");
      hasError = true;
    }

    if (lastName === "") {
      this.setFormErrors("lastName", "Last Name is required.");
      hasError = true;
    }

    if (dob === "") {
      this.setFormErrors("dob", "Date of birth is required.");
      hasError = true;
    }

    if (nativeLanguage === "") {
      this.setFormErrors("nativeLanguage", "Native language is required.");
      hasError = true;
    }

    if (phoneNo === "") {
      this.setFormErrors("phoneNo", "Phone number is required.");
      hasError = true;
    }
    return hasError;
  };

  handleFormSubmit = () => {
    if (!this.validateForm()) {
      const {
        match: {
          params: { courseId }
        },
        course: { schoolId }
      } = this.props;
      const {
        selectedWeek: weeks,
        courseStartDate: startDate,
        form: { email, firstName, lastName, dob, phoneNo },
        accommodation: {
          type,
          startDate: startdate,
          endDate,
          youngChildren,
          olderChildren,
          olderAdults,
          pets,
          smoke,
          foodRestrictions,
          alergies,
          medicalConditions
        }
      } = this.state;

      this.props.bookACourse({
        courseId,
        email,
        accommodation: {
          type,
          startdate,
          endDate,
          youngChildren,
          olderChildren,
          olderAdults,
          pets,
          smoke,
          foodRestrictions,
          alergies,
          medicalConditions
        },
        travelDetails: {},
        bookingSummery: {
          weeks,
          schoolId,
          startDate
        },
        studentInfo: {
          firstName,
          lastName,
          dob,
          email,
          phoneNo
        }
      });
    }
  };

  resetFormErrors = () => {
    this.setState({
      formErrors: {
        email: null,
        firstName: null,
        lastName: null,
        dob: null,
        phoneNo: null,
        nativeLanguage: null
      }
    });
  };

  handleAccommodationPreferences = type => {
    this.setState({
      accommodation: {
        ...this.state.accommodation,
        type: type
      }
    });
  };

  handleHomeStayPreferenceChange = (value, key) => {
    this.setState({
      accommodation: {
        ...this.state.accommodation,
        [key]: value
      }
    });
  };

  setFormErrors = (field, message) => {
    this.setState(state => {
      return {
        formErrors: {
          ...state.formErrors,
          [field]: message
        }
      };
    });
  };

  onRangeSelect = (selectedRange: number) => {
    this.setState({
      selectedRange,
      isRangeSelected: true
    });
  };

  getWeekRange = (
    min: $PropertyType<CourseFeeType, "weeksMin">,
    max: $PropertyType<CourseFeeType, "weeksMax">
  ) => {
    let ranges = [];

    let start = min === null ? 1 : parseInt(min);
    const end = max === null ? 50 : parseInt(max);

    while (start <= end) {
      ranges.push(start);
      start++;
    }

    return ranges;
  };

  handleCourseStartDate = courseStartDate => {
    this.setState({
      courseStartDate: courseStartDate
    });
  };

  handleHomeStayDates = (homeStayStartDate, key) => {
    this.setState({
      accommodation: {
        ...this.state.accommodation,
        [key]: homeStayStartDate
      }
    });
  };

  handleSelectedWeek = selectedWeek => {
    this.setState({
      selectedWeek: selectedWeek
    });
  };

  handleAccomodationSelectedWeek = selectedWeek => {
    this.setState(
      ({ accommodation }) => ({
        accommodation: {
          ...accommodation,
          homeStayWeeks: selectedWeek
        }
      })
    );
  }

  handleNativeLanguageChange = nativeLanguage => {
    this.setState({
      form: {
        ...this.state.form,
        nativeLanguage
      }
    });
  };

  nextStep = () => {
    const { step } = this.state;

    if (step !== this.STEP || !this.validateForm()) {
      this.setState({
        step: step + 1
      });
    }
  };

  prevStep = () => {
    const { step } = this.state;

    this.setState({
      step: step - 1
    });
  };

  onClickHomestayPreference = option => {
    const showPreferences = option !== this.OPTION1 ? true : false;
    this.setState({
      accommodationForm: {
        ...this.state.accommodationForm,
        checked: option,
        showPreferences: showPreferences
      }
    });

    this.handleAccommodationPreferences(
      option === this.OPTION1
        ? this.SELF_ARRANGE
        : option === this.OPTION2
          ? this.UNDER18
          : this.ABOVE18
    );
  };

  handlePaymentMethodChange = method => {
    this.setState(
      ({ payment }) => ({
        payment: {
          ...payment,
          method: method
        }
      })
    );
  }

  getStepForms = () => {
    const { course, loading } = this.props;

    let {
      accommodationForm: { checked, showPreferences, selectOptions },
      form,
      courseStartDate,
      selectedWeek,
      formErrors,
      selectedRange,
      step,
      accommodation
    } = this.state;

    const fees = course.courseFees.map((courseFee, key) => ({
      name:
        courseFee.weeksMin === courseFee.weeksMax
          ? `${courseFee.weeksMin} weeks`
          : `${courseFee.weeksMin} - ${courseFee.weeksMax} weeks`,
      value: key
    }));

    let range = null;
    let options = [];

    if (selectedRange !== null) {
      range = course.courseFees[selectedRange];
      options = this.getWeekRange(range.weeksMin, range.weeksMax);
    }

    const isFixedCourse = range === null || range.weeksMin === null;

    switch (step) {
      case 1:
        return (
          <BookingDetailForm
            course={course}
            courseStartDate={courseStartDate}
            selectedWeek={selectedWeek}
            isFixedCourse={isFixedCourse}
            fees={fees}
            selectedRange={selectedRange}
            range={range}
            options={options}
            onRangeSelect={this.onRangeSelect.bind(this)}
            handleCourseStartDate={this.handleCourseStartDate.bind(this)}
            handleSelectedWeek={this.handleSelectedWeek.bind(this)}
            nextStep={() => this.nextStep()}
          />
        );
      case 2:
        return (
          <AccomodationForm
            course={course}
            checked={checked}
            options={options}
            showPreferences={showPreferences}
            selectOptions={selectOptions}
            onClickHomestayPreference={this.onClickHomestayPreference.bind(
              this
            )}
            handleAccommodationPreferences={this.handleAccommodationPreferences.bind(
              this
            )}
            handleHomeStayPreferenceChange={this.handleHomeStayPreferenceChange.bind(
              this
            )}
            handleAccomodationSelectedWeek={this.handleAccomodationSelectedWeek.bind(
              this
            )}
            selectedWeek={selectedWeek}
            accommodation={accommodation}
            handleHomeStayDates={this.handleHomeStayDates.bind(this)}
            nextStep={() => this.nextStep()}
            prevStep={() => this.prevStep()}
          />
        );
      case 3:
        return (
          <TravelDetailForm
            course={course}
            nextStep={() => this.nextStep()}
            prevStep={() => this.prevStep()}
          />
        );
      case 4:
        return (
          <PersonalInfoForm
            form={form}
            formErrors={formErrors}
            handleFormFieldChange={this.handleFormFieldChange.bind(this)}
            handleNativeLanguageChange={this.handleNativeLanguageChange.bind(
              this
            )}
            nextStep={() => this.nextStep()}
            prevStep={() => this.prevStep()}
          />
        );
      case 5:
        return (
          <PaymentSchemeForm
            handlePaymentMethodChange={this.handlePaymentMethodChange.bind(this)}
            handleFormSubmit={this.handleFormSubmit.bind(this)}
            prevStep={() => this.prevStep()}
            loading={loading}
          />
        );
    }
  };

  render() {
    const {
      course,
      isLoaded,
      passport,
      bookingId,
      isBookingSuccess
    } = this.props;

    let {
      selectedWeek,
      selectedRange,
      step,
      accommodation: { type, homeStayWeeks },
      payment: { method }
    } = this.state;

    let discount = 0;

    if (!isLoaded) {
      return <Loader isLoading />;
    }

    if (bookingId !== null && isBookingSuccess) {
      return (
        <Redirect
          to={{
            pathname: `/booking/payment/${bookingId}`
          }}
        />
      );
    }

    let range = null;

    if (selectedRange !== null) {
      course.discountList.map(discountItem => {
        if (discountItem.weeksMin === null && discountItem.weeksMax === null) {
          if (discountItem.countryOfOrigin.includes(passport)) {
            discount = discountItem.discountedPrice;
          }
        } else {
          if (
            selectedWeek !== null &&
            discountItem.weeksMin <= selectedWeek &&
            discountItem.weeksMax >= selectedWeek &&
            discountItem.countryOfOrigin.includes(passport)
          ) {
            discount = discountItem.discountedPrice;
          }
        }
      });
      range = course.courseFees[selectedRange];
    }

    return (
      <div className="course-booking-page">
        <PageHeader
          title="COURSE BOOKING"
          breadcrumbs={[
            {
              title: "Booking"
            }
          ]}
        />
        <div className="container">
          <Row className="course-details-columns">
            <Col size="8">
              <ProgressBar step={step} />
              <Section>{this.getStepForms()}</Section>
            </Col>
            <Col size="4" className="sidebar-container">
              <Sidebar>
                {selectedWeek !== null && range !== null && (
                  <Widget title="COURSE FEES" className="course-fee-widget">
                    <ul className="course-fees">
                      <li>
                        <span className="course-fee-title">Course fee</span>
                        <span className="course-fee">
                          <Currency currencyType={course.currencyType}>
                            {selectedWeek * range.price}
                          </Currency>
                        </span>
                      </li>
                      <li>
                        <span className="course-fee-title">
                          School registration fee
                        </span>
                        <span className="course-fee">
                          <Currency currencyType={course.currencyType}>
                            {course.enrolmentFee}
                          </Currency>
                        </span>
                      </li>
                      {discount !== 0 && (
                        <Fragment>
                          <li className="regular-price">
                            <span className="course-fee-title">
                              Regular Price
                            </span>
                            <span className="course-fee">
                              <Currency currencyType={course.currencyType}>
                                {selectedWeek * range.price +
                                  course.enrolmentFee}
                              </Currency>
                            </span>
                          </li>
                          <li>
                            <span className="course-fee-title">Savings</span>
                            <span className="course-fee">
                              -
                              <Currency currencyType={course.currencyType}>
                                {selectedWeek * discount}
                              </Currency>
                            </span>
                          </li>
                        </Fragment>
                      )}
                      {type === this.UNDER18 ? (
                        <li>
                          <span className="course-fee-title">
                            Accommodation
                          </span>
                          <span className="course-fee">
                            <Currency currencyType={course.currencyType}>
                              {course.homestays[0].under18Price * homeStayWeeks +
                                course.homestays[0].homestayFee}
                            </Currency>
                          </span>
                        </li>
                      ) : (
                          type === this.ABOVE18 && (
                            <li>
                              <span className="course-fee-title">
                                Accommodation
                            </span>
                              <span className="course-fee">
                                <Currency currencyType={course.currencyType}>
                                  {course.homestays[0].above18Price * homeStayWeeks +
                                    course.homestays[0].homestayFee}
                                </Currency>
                              </span>
                            </li>
                          )
                        )}
                      {method === this.STRIPE && type === this.UNDER18 ? (
                        <li>
                          <span className="course-fee-title">
                            Transaction Charges
                          </span>
                          <span className="course-fee">
                            <Currency currencyType={course.currencyType}>
                              {(selectedWeek * (range.price - discount) +
                                course.enrolmentFee +
                                course.homestays[0].under18Price * homeStayWeeks +
                                course.homestays[0].homestayFee) *
                                0.065}
                            </Currency>
                          </span>
                        </li>
                      ) : method === this.STRIPE && type === this.ABOVE18 ? (
                        <li>
                          <span className="course-fee-title">
                            Transaction Charges
                          </span>
                          <span className="course-fee">
                            <Currency currencyType={course.currencyType}>
                              {(selectedWeek * (range.price - discount) +
                                course.enrolmentFee +
                                course.homestays[0].above18Price * homeStayWeeks +
                                course.homestays[0].homestayFee) *
                                0.065}
                            </Currency>
                          </span>
                        </li>
                      ) : (
                            method === this.STRIPE && (
                              <li>
                                <span className="course-fee-title">
                                  Transaction Charges
                                </span>
                                <span className="course-fee">
                                  <Currency currencyType={course.currencyType}>
                                    {(selectedWeek * (range.price - discount) +
                                      course.enrolmentFee) *
                                      0.065}
                                  </Currency>
                                </span>
                              </li>
                            )
                          )}
                      <li className="total">
                        <span className="course-fee-title">Total</span>
                        <span className="course-fee">
                          <Currency currencyType={course.currencyType}>
                            {method === this.STRIPE && type === this.UNDER18
                              ? (selectedWeek * (range.price - discount) +
                                course.enrolmentFee +
                                course.homestays[0].under18Price * homeStayWeeks +
                                course.homestays[0].homestayFee) *
                              1.065
                              : method === this.STRIPE && type === this.ABOVE18
                                ? (selectedWeek * (range.price - discount) +
                                  course.enrolmentFee +
                                  course.homestays[0].above18Price * homeStayWeeks +
                                  course.homestays[0].homestayFee) *
                                1.065
                                : method === this.STRIPE ?
                                  (selectedWeek * (range.price - discount) +
                                    course.enrolmentFee) *
                                  1.065
                                  : method === "" && type === this.UNDER18
                                    ? (selectedWeek * (range.price - discount) +
                                      course.enrolmentFee +
                                      course.homestays[0].under18Price * homeStayWeeks +
                                      course.homestays[0].homestayFee)
                                    : method === "" && type === this.ABOVE18
                                      ? (selectedWeek * (range.price - discount) +
                                        course.enrolmentFee +
                                        course.homestays[0].above18Price * homeStayWeeks +
                                        course.homestays[0].homestayFee)
                                      : method === "" &&
                                      (selectedWeek * (range.price - discount) +
                                        course.enrolmentFee)
                            }
                          </Currency>
                        </span>
                      </li>
                      <li className="total">
                        <span className="course-fee-title">Actual Course Price</span>
                        <span className="course-fee">
                          <Fragment>
                            {course.currencyType + " "}
                            {method === this.STRIPE && type === this.UNDER18
                              ? (
                                (selectedWeek * (range.price - discount) +
                                  course.enrolmentFee +
                                  course.homestays[0].under18Price * homeStayWeeks +
                                  course.homestays[0].homestayFee) *
                                1.065
                              ).toFixed(2)
                              : method === this.STRIPE && type === this.ABOVE18
                                ? (
                                  (selectedWeek * (range.price - discount) +
                                    course.enrolmentFee +
                                    course.homestays[0].above18Price * homeStayWeeks +
                                    course.homestays[0].homestayFee) *
                                  1.065
                                ).toFixed(2)
                                : method === this.STRIPE
                                  ? (
                                    (selectedWeek * (range.price - discount) +
                                      course.enrolmentFee) *
                                    1.065
                                  ).toFixed(2)
                                  : method === "" && type === this.UNDER18
                                    ? (selectedWeek * (range.price - discount) +
                                      course.enrolmentFee +
                                      course.homestays[0].under18Price * homeStayWeeks +
                                      course.homestays[0].homestayFee).toFixed(2)
                                    : method === "" && type === this.ABOVE18
                                      ? (selectedWeek * (range.price - discount) +
                                        course.enrolmentFee +
                                        course.homestays[0].above18Price * homeStayWeeks +
                                        course.homestays[0].homestayFee).toFixed(2)
                                      : method === "" &&
                                      (selectedWeek * (range.price - discount) +
                                        course.enrolmentFee).toFixed(2)
                            }
                          </Fragment>
                        </span>
                      </li>
                    </ul>
                  </Widget>
                )}
              </Sidebar>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    course: getConsolidatedCourse(state),
    isLoaded: isCourseLoaded(state, props),
    isBookingSuccess: state.booking.isBookingSuccess,
    bookingId: state.booking.bookingId,
    loading: state.booking.loading,
    passport: state.profile.settings.passport
  };
};

const Actions = {
  getCourse,
  bookACourse
};

export default connect(
  mapStateToProps,
  Actions
)(CourseInfo);
