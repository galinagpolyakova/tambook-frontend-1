// @flow
import React from "react";

import { type CourseType } from "../../../course/types";

import RadioButton from "../../../../shared/components/RadioButton";
import Button from "../../../../shared/components/Button";

import "./styles.css";

type TravelDetailFormProps = {
    course: CourseType,
    prevStep: Function,
    nextStep: Function
}

const TravelDetailForm = (props: TravelDetailFormProps) => {
    const {
        course,
        prevStep,
        nextStep
    } = props;
    return (
        <div className="booking-course-travel-details">
            <p className="heading-4">TRAVEL DETAILS</p>
            <div className="booking-course-travel-details-section">
                <p className="heading-6">INSURANCE</p>
                <p>
                    It is a legal requirement for all students studying abroad
                    to have full medical and travel insurance coverage for the
                    entire period they are studying overseas. If you already
                    have insurance or plan to arrange an insurance plan
                    yourself, be sure to check your plan meets your{" "}
                    <a href={course.website}>schools requirements.</a>
                </p>
                <RadioButton
                    label="I'll arrange insurance myself"
                    checked={true}
                />
            </div>
            <div className="booking-course-travel-details-section">
                <p className="heading-6">AIRPORT PICK UP</p>
                <RadioButton
                    label="I'll arrange transportation from the airport myself"
                    checked={true}
                />
            </div>
            <div className="button-block" >
                <Button
                    size={Button.size.MEDIUM}
                    type={Button.type.SECONDARY}
                    onClick={prevStep}
                >
                    STEP BACK
                </Button>
                <Button
                    size={Button.size.MEDIUM}
                    type={Button.type.SECONDARY}
                    onClick={nextStep}
                >
                    NEXT STEP
                </Button>
            </div>
        </div>
    );
};

export default TravelDetailForm;