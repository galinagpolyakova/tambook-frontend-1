// @flow
import React from "react";

import Row from "../../../../shared/components/Row";
import Col from "../../../../shared/components/Col";
import Input from "../../../../shared/components/Input";
import Select from "../../../../shared/components/Select";
import Checkbox from "../../../../shared/components/Checkbox";
import Button from "../../../../shared/components/Button";

import "./styles.css";

type PersonalInfoFormProps = {
    form: Object,
    formErrors: Object,
    handleFormFieldChange: Function,
    handleNativeLanguageChange: Function,
    prevStep: Function,
    nextStep: Function
}

const PersonalInfoForm = (props: PersonalInfoFormProps) => {
    const {
        form,
        formErrors,
        handleFormFieldChange,
        handleNativeLanguageChange,
        prevStep,
        nextStep
    } = props;
    return (
        <div>
            <p className="heading-4">STUDENT PERSONAL INFORMATION</p>
            <Row>
                <Col>
                    <div className="input-group">
                        <label>First Name</label>
                        <Input
                            placeholder="Enter your first name"
                            id="firstName"
                            text={form.firstName}
                            onChange={handleFormFieldChange}
                            error={formErrors.firstName}
                        />
                    </div>
                </Col>
                <Col>
                    <div className="input-group">
                        <label>Last Name</label>
                        <Input
                            placeholder="Enter your last name"
                            id="lastName"
                            text={form.lastName}
                            onChange={handleFormFieldChange}
                            error={formErrors.lastName}
                        />
                    </div>
                </Col>
            </Row>
            <Row>
                <Col>
                    <div className="input-group">
                        <label>Date of Birth</label>
                        <Input
                            placeholder="dd/mm/yyyy"
                            id="dob"
                            text={form.dob}
                            onChange={handleFormFieldChange}
                            error={formErrors.dob}
                        />
                    </div>
                </Col>
                <Col>
                    <div className="input-group">
                        <label>E-mail Address</label>
                        <Input
                            placeholder="Enter your e-mail address"
                            id="email"
                            text={form.email}
                            onChange={handleFormFieldChange}
                            error={formErrors.email}
                        />
                    </div>
                </Col>
            </Row>
            <Row>
                <Col>
                    <div className="input-group">
                        <label>Native Language</label>
                        <Select
                            placeholder="Select a Native Language"
                            options={["English", "French"]}
                            selected={form.nativeLanguage}
                            error={formErrors.nativeLanguage}
                            onChange={nativeLanguage =>
                                handleNativeLanguageChange(nativeLanguage)
                            }
                            id="nativeLanguage"
                        />
                    </div>
                </Col>
                <Col>
                    <div className="input-group">
                        <label>Phone Number</label>
                        <Input
                            placeholder="Enter your phone number"
                            id="phoneNo"
                            text={form.phoneNo}
                            onChange={handleFormFieldChange}
                            error={formErrors.phoneNo}
                        />
                    </div>
                </Col>
            </Row>
            <Row>
                <Col>
                    <Checkbox text="I want to receive Tambook promotional offers in the future" />
                </Col>
            </Row>
            <div className="button-block">
                <Button
                    size={Button.size.MEDIUM}
                    type={Button.type.SECONDARY}
                    onClick={prevStep}
                >
                    STEP BACK
                </Button>
                <Button
                    size={Button.size.MEDIUM}
                    type={Button.type.SECONDARY}
                    onClick={nextStep}
                >
                    NEXT STEP
                </Button>
            </div>
        </div>
    );
};

export default PersonalInfoForm;