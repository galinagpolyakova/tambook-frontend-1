import {
  BOOK_A_COURSE_INIT,
  BOOK_A_COURSE_SUCCESS,
  BOOK_A_COURSE_FAILURE,
  FINALIZE_PAYMENT_SUCCESS,
  FINALIZE_PAYMENT_FAILURE,
  FINALIZE_PAYMENT_INIT,
  SHOW_PAYMENT_ERROR
} from "./actionTypes";

// $FlowFixMe
export function bookACourse(bookingSummery) {
  return (dispatch, getState, serviceManager) => {
    dispatch(bookACourseInit());

    let bookingService = serviceManager.get("BookingService");
    bookingService
      .bookACourse(bookingSummery)
      .then(({ result: { bookingId } }) =>
        dispatch(
          bookACourseSuccess({
            bookingId
          })
        )
      )
      .catch(err => {
        dispatch(bookACourseFailure(err));
      });
  };
}

function bookACourseInit() {
  return {
    type: BOOK_A_COURSE_INIT
  };
}
function bookACourseSuccess(payload) {
  return {
    type: BOOK_A_COURSE_SUCCESS,
    payload
  };
}
function bookACourseFailure() {
  return {
    type: BOOK_A_COURSE_FAILURE
  };
}

export function finalizePayment(bookingSummery) {
  return (dispatch, getState, serviceManager) => {
    dispatch(finalizePaymentInit());

    let bookingService = serviceManager.get("BookingService");

    bookingService
      .finalizePayment({ bookingSummery })
      .then(({ result: { bookingId, courseId } }) =>
        dispatch(
          finalizePaymentSuccess({
            bookingId,
            courseId
          })
        )
      )
      .catch(({ message: error }) => {
        dispatch(finalizePaymentFailure({ error }));
      });
  };
}

function finalizePaymentSuccess() {
  return {
    type: FINALIZE_PAYMENT_SUCCESS
  };
}
function finalizePaymentFailure(payload) {
  return {
    type: FINALIZE_PAYMENT_FAILURE,
    payload
  };
}
function finalizePaymentInit() {
  return {
    type: FINALIZE_PAYMENT_INIT
  };
}

export function showPaymentError(payload) {
  return {
    type: SHOW_PAYMENT_ERROR,
    payload
  };
}
