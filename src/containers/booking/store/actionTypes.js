const PREFIX = "@booking/";

export const BOOK_A_COURSE_INIT = `${PREFIX}BOOK_A_COURSE_INIT`;
export const BOOK_A_COURSE_SUCCESS = `${PREFIX}BOOK_A_COURSE_SUCCESS`;
export const BOOK_A_COURSE_FAILURE = `${PREFIX}BOOK_A_COURSE_FAILURE`;

export const FINALIZE_PAYMENT_SUCCESS = `${PREFIX}FINALIZE_PAYMENT_SUCCESS`;
export const FINALIZE_PAYMENT_FAILURE = `${PREFIX}FINALIZE_PAYMENT_FAILURE`;
export const FINALIZE_PAYMENT_INIT = `${PREFIX}FINALIZE_PAYMENT_INIT`;

export const SHOW_PAYMENT_ERROR = `${PREFIX}SHOW_PAYMENT_ERROR`;
