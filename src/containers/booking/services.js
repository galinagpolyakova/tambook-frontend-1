// @flow
import type { ApiServiceInterface } from "../../shared/services/ApiServiceInterface";

export class BookingService {
  api: ApiServiceInterface;

  endpoint: string = "/booking";

  constructor(apiService: ApiServiceInterface) {
    this.api = apiService;
  }

  _normalizeBooking(apiBooking: Object) {
    return {
      id: apiBooking.bookingId,
      title: apiBooking.course.name,
      duration: apiBooking.bookingSummery.weeks,
      date: apiBooking.createdDate,
      status: apiBooking.paymentStatus
    };
  }

  _normalizeBookings(apiBookings: Array<Object>) {
    const bookings = [];
    apiBookings.map(apiBooking => {
      let course = this._normalizeBooking(apiBooking._source);
      bookings.push(course);
    });
    return {
      bookings
    };
  }

  async request(endpoint: string, query: Object = {}, options: Object = {}) {
    let response = await this.api.get(endpoint, query, options);

    return response;
  }

  bookACourse(payload: Object) {
    return this.api.put(this.endpoint, payload);
  }

  finalizePayment(payload: Object) {
    return this.api.post(`${this.endpoint}/payment`, payload);
  }

  getBookings() {
    return this.api
      .get(this.endpoint)
      .then(({ result }) => this._normalizeBookings(result));
  }
}
