// @flow
import { createSelector } from "reselect";
import { type ApplicationState } from "../../../store/reducers";
import { type CourseDetailsProps } from "../pages/details";
import { type ConsolidatedCourseType } from "../types";

export const getCourses = (state: ApplicationState) => state.course.courses;

export const getRelatedCourses = (state: ApplicationState) =>
  state.course.course.relatedCources;

export const getCourseParams = (state: ApplicationState) =>
  state.course.filters;

export const isCourseParamLoaded = (createSelector(
  [getCourseParams],
  (courseParams): boolean => {
    return courseParams !== null;
  }
): (state: ApplicationState) => boolean);

export const getCourse = (state: ApplicationState) => state.course.course;

export const getCourseData = (state: ApplicationState) =>
  state.course.course.data;

export const getCourseId = (
  state: ApplicationState,
  props: CourseDetailsProps
) => props.match.params.courseId;

export const isCourseLoaded = (createSelector(
  [getCourse, getCourseId],
  (course, courseId): boolean => {
    if (course.id === courseId) {
      return true;
    }
    return false;
  }
): (state: ApplicationState, props: CourseDetailsProps) => boolean);

export const isCoursePageLoaded = (createSelector(
  [getCourses, getCourseParams],
  (courses, filters): boolean => {
    if (courses.length === 0 || filters === null) {
      return false;
    }
    return true;
  }
): (state: ApplicationState) => boolean);

export const isCourseParamsLoaded = (createSelector(
  [getCourseParams],
  (filters): boolean => {
    if (filters === null) {
      return false;
    }
    return true;
  }
): (state: ApplicationState) => boolean);

export const getCourseBookmarkList = (state: ApplicationState) =>
  state.course.bookmarkList;

export const getCourseCompareList = (state: ApplicationState) =>
  state.course.compareList;

export const isCourseBookmarksListLoaded = (createSelector(
  [getCourseBookmarkList],
  (bookmarkList): boolean => {
    return bookmarkList !== null;
  }
): (state: ApplicationState) => boolean);

export const getConsolidatedCourses = createSelector(
  [getCourses, getCourseBookmarkList, getCourseCompareList],
  (courses, bookmarkedCourses, comparedCourses) => {
    return getConsolidatedCoursesFromStore(
      courses,
      bookmarkedCourses,
      comparedCourses
    );
  }
);

export const getConsolidatedRelatedCourses = createSelector(
  [getRelatedCourses, getCourseBookmarkList, getCourseCompareList],
  (courses, bookmarkedCourses, comparedCourses) => {
    return getConsolidatedCoursesFromStore(
      courses,
      bookmarkedCourses,
      comparedCourses
    );
  }
);

export const getConsolidatedCourse = (createSelector(
  [getCourseData, getCourseBookmarkList, getCourseCompareList],
  (course, bookmarkedCourses, comparedCourses) => {
    if (course !== null) {
      return getConsolidatedCourseFromStore(
        course,
        bookmarkedCourses,
        comparedCourses
      );
    }
  }
): (state: ApplicationState) => ConsolidatedCourseType | typeof undefined);

function getConsolidatedCoursesFromStore(
  courses,
  bookmarkedCourses,
  comparedCourses
) {
  const consolidatedCourses = [];
  for (let course of courses) {
    consolidatedCourses.push(
      getConsolidatedCourseFromStore(course, bookmarkedCourses, comparedCourses)
    );
  }
  return consolidatedCourses;
}

function getConsolidatedCourseFromStore(
  course,
  bookmarkedCourses,
  comparedCourses
) {
  if (bookmarkedCourses !== null) {
    let bookmarkedCourse = bookmarkedCourses.find(courseItem => {
      return course.id === courseItem;
    });
    course.isBookmarked = bookmarkedCourse !== undefined;
  }
  let comparedCourse = comparedCourses.find(courseItem => {
    return course.id === courseItem;
  });
  course.isAddedToCompare = comparedCourse !== undefined;
  return course;
}
