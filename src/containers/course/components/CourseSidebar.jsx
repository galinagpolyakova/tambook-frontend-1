// @flow
import type { CourseType } from "../types";

import React, { PureComponent } from "react";

import * as countries from "../../../shared/data/countries.json";

import Button from "../../../shared/components/Button";
import Sidebar, { Widget } from "../../../components/Sidebar";
import Currency from "../../../components/Currency";
import Rate from "../../../shared/components/Rate";

//import Homestay from "../../../components/Homestay/Homestay";
import InfoWidget from "../../../components/infoWidget";
import CourseFee from "./CourseFee";
import Map from "../../../shared/components/Map.jsx";

type CourseSidebarProps = {
  courseId: number,
  location: {
    city: string,
    country: string
  },
  courseFees: $PropertyType<CourseType, "courseFees">,
  workEligibility: $PropertyType<CourseType, "workEligibility">,
  passport: string,
  language: string,
  age: $PropertyType<CourseType, "minAge">,
  rating: number,
  isBookmarked: boolean,
  isBookmarkLoading: boolean,
  price: $PropertyType<CourseType, "startingPrice">,
  googleMap: $PropertyType<CourseType, "googleMap">,
  homestays: $PropertyType<CourseType, "homestays">,
  onAddToList: Function,
  onRemoveFromList: Function,
  enrolmentFee: $PropertyType<CourseType, "enrolmentFee">,
  bookACourse: Function,
  selectedCurrency: string,
  discountList: $PropertyType<CourseType, "discountList">,
  isPaymentEnabled: $PropertyType<CourseType, "isPaymentEnabled">,
  currencyType: $PropertyType<CourseType, "currencyType">,
  googleCordinates: $PropertyType<CourseType, "googleCordinates">
};

class CourseSidebar extends PureComponent<CourseSidebarProps> {
  render() {
    const {
      courseId,
      location,
      courseFees,
      workEligibility,
      passport,
      language,
      age,
      rating,
      price,
      googleMap,
      // homestays,
      isBookmarked,
      enrolmentFee,
      bookACourse,
      selectedCurrency,
      discountList,
      isPaymentEnabled,
      currencyType,
      googleCordinates,
      isBookmarkLoading
    } = this.props;
    return (
      <Sidebar>
        <Widget className="filter">
          <div className="country">
            PASSPORT:{" "}
            <span>
              {passport === "" ? "Not selected" : countries[passport]}
            </span>
          </div>
          <div className="courency">
            CURRENCY: <span>{selectedCurrency}</span>
          </div>
        </Widget>
        <Widget title="DETAILS" className="summary">
          <div className="summary-line">
            <span>LANGUAGE: </span> {language}
          </div>
          <div className="summary-line">
            <span>PRICE:</span>{" "}
            <Currency currencyType={currencyType}>{price}</Currency> / WEEK
          </div>
          <div className="summary-line">
            <span>LOCATION: </span> {location.city}
            ,&nbsp;
            {location.country}
          </div>
          <div className="summary-line">
            <span>ELEGABLE FOR WORK:</span> {workEligibility ? "YES" : "NO"}
          </div>
          <div className="summary-line">
            <span>Rate:</span>
            <div className="ratings">
              <Rate value={rating} />
            </div>
          </div>
          <div className="summary-line">
            <span>MIN AGE:</span> {age} YEARS
          </div>
          <Button
            onClick={() =>
              isBookmarked
                ? this.props.onRemoveFromList()
                : this.props.onAddToList()
            }
            size={Button.size.SMALL}
            className="add-to-list"
            loading={isBookmarkLoading}
          >
            <i />
            {isBookmarked ? "REMOVE FROM LIST" : "ADD TO LIST"}
          </Button>
        </Widget>

        <CourseFee
          fees={courseFees}
          courseId={courseId}
          discountList={discountList}
          enrolmentFee={enrolmentFee}
          bookACourse={bookACourse}
          passport={passport}
          isPaymentEnabled={isPaymentEnabled}
          currencyType={currencyType}
        />
        {googleCordinates.latitude !== null &&
          googleCordinates.longitude !== null ? (
            <Widget>
              <div style={{ minHeight: "200px", width: "100%" }}>
                <Map
                  lat={googleCordinates.latitude}
                  lng={googleCordinates.longitude}
                />
              </div>
            </Widget>
          ) : (
            googleMap !== null && (
              <Widget>
                <iframe
                  title="map"
                  src={googleMap}
                  width="100%"
                  height="250"
                  frameBorder="0"
                />
              </Widget>
            )
          )}
        {/*<Widget
          title="ACCOMODATION"
          type={Widget.types.LIGHT}
          className="homestay-widget"
        >
          {homestays.map((homestay, key) => (
            <Homestay key={key} {...homestay} currencyType={currencyType} />
          ))}
          </Widget>*/}
        <Widget>
          <Button
            htmlType={Button.htmlType.LINK}
            link="/under-construction"
            fullWidth
          >
            CHECK VISA REQUIREMENTS
          </Button>
        </Widget>
        <InfoWidget />
      </Sidebar>
    );
  }
}

export default CourseSidebar;
