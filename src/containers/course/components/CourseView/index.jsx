// @flow
import type {
  ConsolidatedCourseType,
  ConsolidatedCoursesListType
} from "../../types";
import React, { PureComponent, Fragment } from "react";

import CourseGridItem from "../../../../components/Course";
import CourseListItem from "./CourseListItem";
import SortButton from "../../../../shared/components/SortButton";
import {
  ItemsLayout,
  ItemsLayoutToggler
} from "../../../../shared/components/ItemsLayout";
import Pagination, {
  type PaginationType
} from "../../../../shared/components/Pagination";

import { UserSettingsConsumer } from "../../../../contexts/UserSettings";

import "./styles.css";

type CourseViewProps = {
  courses: ConsolidatedCoursesListType,
  addCourseToBookmarkList: Function,
  removeCourseFromBookmarkList: Function,
  addCourseToCompare: Function,
  removeCourseFromCompare: Function,
  filter: Function,
  pagination: PaginationType,
  onPageChanged: Function
};

class CourseView extends PureComponent<CourseViewProps> {
  render() {
    const {
      courses,
      filter,
      pagination,
      onPageChanged,
      addCourseToCompare,
      removeCourseFromCompare,
      addCourseToBookmarkList,
      removeCourseFromBookmarkList
    } = this.props;
    return (
      <UserSettingsConsumer>
        {({ viewMode, changeViewMode }) => (
          <Fragment>
            <div className="page-filters">
              <div className="sort-buttons">
                <SortButton onClick={filter} filter="priceSort">
                  Sort by price
                </SortButton>
                <SortButton onClick={filter} filter="ratingSort">
                  Sort by ranking
                </SortButton>
              </div>
              <div className="layout-switcher">
                <div>VIEW: </div>
                <ItemsLayoutToggler
                  viewMode={viewMode}
                  changeViewMode={changeViewMode}
                />
              </div>
            </div>
            <ItemsLayout
              items={courses}
              viewMode={viewMode}
              gridByLine={3}
              gridItemComponent={(props: { item: ConsolidatedCourseType }) => {
                return (
                  <CourseGridItem
                    {...props.item}
                    addCourseToBookmarkList={addCourseToBookmarkList}
                    removeCourseFromBookmarkList={removeCourseFromBookmarkList}
                    addCourseToCompare={addCourseToCompare}
                    removeCourseFromCompare={removeCourseFromCompare}
                  />
                );
              }}
              listItemComponent={(props: { item: ConsolidatedCourseType }) => {
                return (
                  <CourseListItem
                    {...props.item}
                    addCourseToBookmarkList={addCourseToBookmarkList}
                    removeCourseFromBookmarkList={removeCourseFromBookmarkList}
                    addCourseToCompare={addCourseToCompare}
                    removeCourseFromCompare={removeCourseFromCompare}
                  />
                );
              }}
            />
            {pagination !== null && (
              <Pagination
                totalPages={pagination.pages}
                currentPage={pagination.currentPage}
                onPageChanged={onPageChanged}
              />
            )}
          </Fragment>
        )}
      </UserSettingsConsumer>
    );
  }
}

export default CourseView;
