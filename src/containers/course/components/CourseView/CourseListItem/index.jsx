// @flow
import { type ConsolidatedCourseType } from "../../../types";

import React, { PureComponent } from "react";
import classNames from "classnames";

import { Rating } from "../../../../../shared/components/Rate";
import Col from "../../../../../shared/components/Col";
import Row from "../../../../../shared/components/Row";
import Button from "../../../../../shared/components/Button";
import IconButton from "../../../../../shared/components/IconButton";

import { isNotEmpty } from "../../../../../shared/utils";

import missingImage from "../../../../../assets/missing-course.jpg";

import "./styles.css";
import Currency from "../../../../../components/Currency";

type CourseProps = {
  id: $PropertyType<ConsolidatedCourseType, "id">,
  title: $PropertyType<ConsolidatedCourseType, "title">,
  location: $PropertyType<ConsolidatedCourseType, "location">,
  price: $PropertyType<ConsolidatedCourseType, "price">,
  image: $PropertyType<ConsolidatedCourseType, "image">,
  rating: $PropertyType<ConsolidatedCourseType, "rating">,
  description: $PropertyType<ConsolidatedCourseType, "description">,
  duration: $PropertyType<ConsolidatedCourseType, "duration">,
  isBookmarked: $PropertyType<ConsolidatedCourseType, "isBookmarked">,
  currencyType: $PropertyType<ConsolidatedCourseType, "currencyType">,
  addCourseToBookmarkList: Function,
  removeCourseFromBookmarkList: Function
};

class Course extends PureComponent<CourseProps> {
  addCourseToBookmarkList = (
    courseId: $PropertyType<ConsolidatedCourseType, "id">
  ) => {
    if (this.props.addCourseToBookmarkList) {
      this.props.addCourseToBookmarkList(courseId);
    }
  };

  removeCourseFromBookmarkList = (
    courseId: $PropertyType<ConsolidatedCourseType, "id">
  ) => {
    if (this.props.removeCourseFromBookmarkList) {
      this.props.removeCourseFromBookmarkList(courseId);
    }
  };

  render() {
    const {
      id,
      title,
      location,
      price,
      image,
      rating,
      description,
      duration,
      isBookmarked,
      currencyType
    } = this.props;
    const courseImage = isNotEmpty(image) ? image : missingImage;
    return (
      <div className="course-list-item">
        <div className="featured-image">
          <img src={courseImage} alt={title} />
        </div>
        <div className="course-content">
          <Row className="detail-block">
            <Col size="8">
              <div className="course-duration">{duration} hours / week</div>
              <div className="course-title">{title}</div>
              <div className="location-title">
                {location.city}, {location.country}
              </div>
              <Rating rate={rating.rate} reviews={rating.reviews} />
              <div className="course-description">{description}</div>
            </Col>
            <Col size="4" className="course-cta">
              <div className="price">
                FROM{" "}
                <span>
                  <Currency currencyType={currencyType}>{price}</Currency>
                </span>
                WEEK
              </div>
              <div className="button-group">
                <IconButton icon="exchange-alt" onClick={() => {}} />
                <IconButton
                  icon="bookmark"
                  className={classNames({ active: isBookmarked })}
                  onClick={() =>
                    isBookmarked
                      ? this.removeCourseFromBookmarkList(id)
                      : this.addCourseToBookmarkList(id)
                  }
                />
              </div>
              <Button
                type={Button.type.SECONDARY}
                size={Button.size.SMALL}
                htmlType={Button.htmlType.LINK}
                link={`/course/${id}`}
              >
                MORE INFO
              </Button>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

export default Course;
