// @flow
import React, { PureComponent } from "react";
import { connect } from "react-redux";
import Course from "../../../components/Course";
import Row from "../../../shared/components/Row";
import Col from "../../../shared/components/Col";
import type { ConsolidatedCoursesListType } from "../types";
import Section, {
  SectionBody,
  SectionHeader
} from "../../../shared/components/Section";
import { getRelatedCourses } from "../store/actions";
import { getConsolidatedRelatedCourses } from "../store/selectors";

type RelatedCourcesProps = {
  courses: ConsolidatedCoursesListType,
  getRelatedCourses: Function,
  addCourseToBookmarkList: Function,
  removeCourseFromBookmarkList: Function,
  country: string,
  language: string
};

class RelatedCources extends PureComponent<RelatedCourcesProps> {
  componentDidMount() {
    const { country, language } = this.props;
    this.props.getRelatedCourses({
      country,
      language
    });
  }
  render() {
    const {
      courses,
      addCourseToBookmarkList,
      removeCourseFromBookmarkList
    } = this.props;
    const rendered = courses.map((item, index) => {
      return (
        <Col size="3" key={index}>
          <Course
            {...item}
            addCourseToBookmarkList={addCourseToBookmarkList}
            removeCourseFromBookmarkList={removeCourseFromBookmarkList}
          />
        </Col>
      );
    });
    return (
      courses.length > 0 && (
        <Section className="realated-courses">
          <SectionHeader position="left" heading="MAY INTEREST YOU" />
          <SectionBody>
            <Row>{rendered}</Row>
          </SectionBody>
        </Section>
      )
    );
  }
}
function mapStateToProps(state) {
  return {
    courses: getConsolidatedRelatedCourses(state)
  };
}

const Action = { getRelatedCourses };

export default connect(
  mapStateToProps,
  Action
)(RelatedCources);
