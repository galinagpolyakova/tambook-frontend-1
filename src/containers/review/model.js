// @flow
import type { ReviewType, ApiReviewType } from "./types";
import type { SerializableInterface } from "../../shared/SerializableInterface";
import image from "../../assets/home/countries/new-zealand/wellington.jpg";
import { SITE_URL } from "../../config/app";

export class Review implements SerializableInterface<ReviewType> {
  name: $PropertyType<ReviewType, "name">;
  profilePic: $PropertyType<ReviewType, "profilePic">;
  tag: $PropertyType<ReviewType, "tag">;
  comment: $PropertyType<ReviewType, "comment">;
  rating: $PropertyType<ReviewType, "rating">;
  image: $PropertyType<ReviewType, "image">;

  constructor(apiReview: ApiReviewType) {
    this.name = apiReview.name;
    // FIXME: when api is finalized
    this.profilePic = `${SITE_URL}${apiReview.profile_pic}`;
    this.tag = apiReview.tag;
    this.comment = apiReview.comment_en;
    this.rating = apiReview.rating;
    // FIXME: when api is finalized
    this.image = image;
  }

  static fromReviewApi(apiReview: ApiReviewType) {
    return new this(apiReview);
  }

  toJSON(): ReviewType {
    return this;
  }
}
