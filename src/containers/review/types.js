// @flow

export type ReviewsListType = Array<ReviewType>;

export type ReviewType = {
  name: string,
  profilePic: string,
  tag: string,
  comment: string,
  rating: number,
  image: string
};

export type ApiReviewsListType = Array<ApiReviewType>;

export type ApiReviewType = {
  active: string,
  comment_es: string,
  rating: number,
  comment_en: string,
  comment_ru: string,
  comment_pt: string,
  tag: string,
  id: number,
  country: string,
  name: string,
  profile_pic: string
};
