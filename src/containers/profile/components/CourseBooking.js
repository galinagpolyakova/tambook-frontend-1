// @flow
import { type BookingList } from "../types";

import React, { PureComponent } from "react";
import Row from "../../../shared/components/Row";
import Col from "../../../shared/components/Col";
import Button from "../../../shared/components/Button";
import format from "../../../shared/utils/dates/format";

type CourseBookingProps = {
  bookings: BookingList
};

class CourseBooking extends PureComponent<CourseBookingProps> {
  render() {
    return (
      <div className="course-booking">
        <div className="course-booking-header">
          <Row>
            <Col size={5}>Title</Col>
            <Col size={2}>Duration (weeks)</Col>
            <Col size={2}>Date</Col>
            <Col size={1}>Status</Col>
            <Col size={2} className="text-center">
              Details
            </Col>
          </Row>
        </div>
        <div className="course-booking-body">
          {this.props.bookings.map((booking, key) => (
            <Row key={key} className="course-booking-row">
              <Col size={5}>{booking.title}</Col>
              <Col size={2}>{booking.duration} (weeks)</Col>
              <Col size={2}>{format(booking.date, "yyyy-MM-dd")}</Col>
              <Col size={1} className="status text-capitalize">
                {booking.status}
              </Col>
              <Col size={2} className="text-center">
                <Button
                  size={Button.size.SMALL}
                  htmlType={Button.htmlType.LINK}
                  link={`/profile/bookings/${booking.id}`}
                >
                  Details
                </Button>
              </Col>
            </Row>
          ))}
        </div>
      </div>
    );
  }
}

export default CourseBooking;
