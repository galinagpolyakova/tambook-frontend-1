// @flow
import type { Node } from "react";

import React, { PureComponent } from "react";
import { Link } from "react-router-dom";
import classnames from "classnames";

import PageHeader from "../../../components/PageHeader";

import "../styles.css";

type LayoutProps = {
  currentLink: string,
  currentTitle: string,
  children: Node
};

class Layout extends PureComponent<LayoutProps> {
  render() {
    const { currentLink, currentTitle, children } = this.props;
    const pages = [
      { link: "bookings", title: "Bookings" },
      { link: "bookmarks", title: "Bookmarks" },
      { link: "user-information", title: "User information" },
      { link: "payment-method", title: "Payment method" }
      // { link: "notifications", title: "Notifications" }
    ];
    return (
      <div className="profile-page">
        <PageHeader
          title={currentTitle}
          breadcrumbs={[
            {
              title: "Profile"
            },
            {
              title: currentTitle
            }
          ]}
        />
        <div className="container">
          <div className="header-menu">
            <ul>
              {pages.map(({ title, link }, key) => (
                <li
                  key={key}
                  className={classnames({ active: link === currentLink })}
                >
                  <Link to={`/profile/${link}`}>{title}</Link>
                </li>
              ))}
            </ul>
          </div>
          {children}
        </div>
      </div>
    );
  }
}

export default Layout;
