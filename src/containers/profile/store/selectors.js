// @flow
import type { ApplicationState } from "../../../store/reducers";
import { createSelector } from "reselect";

export const getCourseBookmarkList = (state: ApplicationState) =>
  state.profile.bookmarkedCourses.courses;

export const getCourseCompareList = (state: ApplicationState) =>
  state.course.compareList;

export const isCourseBookmarksListLoaded = (createSelector(
  [getCourseBookmarkList],
  (bookmarkedCourses): boolean => {
    return bookmarkedCourses.length > 0;
  }
): (state: ApplicationState) => boolean);

export const getConsolidatedCourseBookmarkList = createSelector(
  [getCourseBookmarkList, getCourseCompareList],
  (courses, comparedCourses) => {
    const consolidatedCourses = [];
    for (let course of courses) {
      course.isBookmarked = true;
      consolidatedCourses.push(course);
      let comparedCourse = comparedCourses.find(courseItem => {
        return course.id === courseItem;
      });
      course.isAddedToCompare = comparedCourse !== undefined;
    }
    return consolidatedCourses;
  }
);
