// @flow
import React, { Component, Fragment } from "react";

import Input from "../../../shared/components/Input";
import Button from "../../../shared/components/Button";
import Icon from "../../../shared/components/Icon";
import Alert from "../../../shared/components/Alert";

import { isEmail } from "../../../shared/kernel/cast";
import { AUTH_TYPE } from "../constants";
import { hasWhiteSpace } from "../../../shared/utils";

type ForgotPasswordProps = {
  forgotPassword: Function,
  forgotPasswordSubmit: Function,
  toggleAuthType: Function,
  notifications: null | Object,
  isSubmitted: boolean,
  isLoading: boolean,
  isCapsLockActive: boolean
};

type ForgotPasswordState = {
  values: {
    username: string,
    code: string,
    new_password: string,
    confirm_new_password: string
  },
  errors: {
    username: null | string,
    code: null | string,
    new_password: null | string,
    confirm_new_password: null | string
  }
};

class ForgotPassword extends Component<
  ForgotPasswordProps,
  ForgotPasswordState
> {
  state = {
    isSubmitted: false,
    values: {
      username: "",
      code: "",
      new_password: "",
      confirm_new_password: ""
    },
    errors: {
      username: null,
      code: null,
      new_password: null,
      confirm_new_password: null
    }
  };

  constructor(props: ForgotPasswordProps) {
    super(props);
    // $FlowFixMe
    this._resetForm = this._resetForm.bind(this);
    // $FlowFixMe
    this._validateForm = this._validateForm.bind(this);
    // $FlowFixMe
    this._handleInputChange = this._handleInputChange.bind(this);
    // $FlowFixMe
    this._setFormErrors = this._setFormErrors.bind(this);
    // $FlowFixMe
    this._handleFormSubmit = this._handleFormSubmit.bind(this);
    // $FlowFixMe
    this._getNotifications = this._getNotifications.bind(this);
  }

  componentDidMount() {
    this._resetForm();
  }

  _resetForm() {
    this.setState({
      errors: {
        username: null,
        code: null,
        new_password: null,
        confirm_new_password: null
      }
    });
  }

  _validateForm() {
    const {
      values: { username, code, new_password, confirm_new_password }
    } = this.state;
    let hasError = false;

    this._resetForm();
    if (username === "") {
      this._setFormErrors("username", "Email is required.");
      hasError = true;
    } else if (hasWhiteSpace(username)) {
      this._setFormErrors("username", "Email connot contain spaces.");
      hasError = true;
    } else if (!isEmail(username)) {
      this._setFormErrors("username", "Email is invalid.");
      hasError = true;
    }

    if (this.props.isSubmitted) {
      if (code === "") {
        this._setFormErrors("code", "Verification code is required.");
        hasError = true;
      } else if (hasWhiteSpace(code)) {
        this._setFormErrors("code", "Code connot contain spaces.");
        hasError = true;
      }

      if (new_password === "") {
        this._setFormErrors("new_password", "New password is required.");
        hasError = true;
      } else if (
        !/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-.]).{8,20}$/.test(
          new_password
        )
      ) {
        this._setFormErrors(
          "new_password",
          "Password must contain at least 8 characters including a number, a special character, lower case and upper case letters."
        );
        hasError = true;
      } else if (hasWhiteSpace(new_password)) {
        this._setFormErrors("new_password", "Password connot contain spaces.");
        hasError = true;
      }

      if (confirm_new_password === "") {
        this._setFormErrors(
          "confirm_new_password",
          "Confirm password is required."
        );
        hasError = true;
      } else if (confirm_new_password !== new_password) {
        this._setFormErrors(
          "confirm_new_password",
          "The new password and confirmation password do not match."
        );
        hasError = true;
      }
    }
    return hasError;
  }

  _handleInputChange(event: SyntheticInputEvent<HTMLInputElement>) {
    this.setState({
      values: {
        ...this.state.values,
        [event.target.id]: event.target.value
      }
    });
  }

  _setFormErrors(field: string, message: string) {
    this.setState(state => ({
      errors: {
        ...state.errors,
        [field]: message
      }
    }));
  }

  _handleFormSubmit() {
    if (!this._validateForm()) {
      const { username, code, new_password } = this.state.values;
      const isSubmitted = this.props;
      if (!isSubmitted) {
        this.props.forgotPasswordSubmit({
          username
        });
      } else {
        this.props.forgotPassword({
          username,
          code,
          new_password
        });
      }
    }
  }

  _getNotifications() {
    if (this.props.notifications !== null) {
      return this.props.notifications.message;
    }
    return null;
  }

  render() {
    let { isSubmitted, isCapsLockActive, isLoading } = this.props;
    return (
      <Fragment>
        <form autoComplete="off">
          <div className="form-group">
            <label>Email</label>
            <Input
              id="username"
              placeholder="Eg: user@example.com"
              onChange={this._handleInputChange}
              error={this.state.errors.username}
              autoComplete={false}
            />
          </div>
          {isSubmitted && (
            <Fragment>
              <div className="form-group">
                <label>Verification Code</label>
                <Input
                  id="code"
                  placeholder="Eg: 123456"
                  onChange={this._handleInputChange}
                  error={this.state.errors.code}
                  autoComplete={false}
                  name="code"
                />
              </div>
              <div className="form-group">
                <label>New Password</label>
                <Input
                  id="new_password"
                  type="password"
                  placeholder="at least 6 characters including a number, a special character, lower case and upper case letters."
                  onChange={this._handleInputChange}
                  error={this.state.errors.new_password}
                  autoComplete={false}
                  name="password"
                />
                {isCapsLockActive && (
                  <span className="warning">
                    <Icon icon="exclamation-circle" /> Warning: Caps lock
                    enabled.
                  </span>
                )}
              </div>
              <div className="form-group">
                <label>Confirm new Password</label>
                <Input
                  id="confirm_new_password"
                  type="password"
                  placeholder="at least 6 characters including a number, a special character, lower case and upper case letters."
                  onChange={this._handleInputChange}
                  error={this.state.errors.confirm_new_password}
                  autoComplete={false}
                  name="confirm_password"
                />
                {isCapsLockActive && (
                  <span className="warning">
                    <Icon icon="exclamation-circle" /> Warning: Caps lock
                    enabled.
                  </span>
                )}
              </div>
            </Fragment>
          )}
        </form>
        {this._getNotifications()}
        <div className="secondary-link">
          Go back to
          <span
            onClick={() => this.props.toggleAuthType(AUTH_TYPE.SIGNIN)}
            className="link"
          >
            Login
          </span>
        </div>
        {isSubmitted && (
          <Alert type={Alert.type.SUCCSESS}>
            Please check your email account, We have sent you a verification
            code to reset your password!
          </Alert>
        )}
        <div className="button-container">
          <Button
            htmlType={Button.htmlType.BUTTON}
            onClick={() => this._handleFormSubmit()}
            loading={isLoading}
          >
            SUBMIT
          </Button>
        </div>
      </Fragment>
    );
  }
}

export default ForgotPassword;
