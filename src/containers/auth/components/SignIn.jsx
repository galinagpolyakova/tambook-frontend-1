// @flow
import React, { Component, Fragment } from "react";
//import { withFederated } from "aws-amplify-react";

import Input from "../../../shared/components/Input";
import Icon from "../../../shared/components/Icon";
import Button from "../../../shared/components/Button";
import Checkbox from "../../../shared/components/Checkbox";

import config from "../../../config/amplify";
import { hasWhiteSpace } from "../../../shared/utils";

import { isEmail } from "../../../shared/kernel/cast";
import { AUTH_TYPE } from "../constants";

type SignInProps = {
  onFormSubmit: Function,
  toggleAuthType: Function,
  isLoading: boolean,
  handleAuthStateChange: Function,
  notifications: null | Object,
  isCapsLockActive: boolean
};

type SignInState = {
  values: {
    username: string,
    password: string
  },
  errors: {
    username: null | string,
    password: null | string
  },
  rememberMe: boolean
};

class SignIn extends Component<SignInProps, SignInState> {
  state = {
    values: {
      username: "",
      password: ""
    },
    errors: {
      username: null,
      password: null
    },
    rememberMe: true
  };

  componentDidMount() {
    this._loadFacebookSDK();
    this._resetForm();
  }

  _resetForm = () => {
    this.setState({
      errors: {
        username: null,
        password: null
      }
    });
  };

  _validateForm = () => {
    const { username, password } = this.state.values;

    let hasError = false;

    this._resetForm();

    if (username === "") {
      this.setFormErrors("username", "Email is required.");
      hasError = true;
    } else if (hasWhiteSpace(username)) {
      this.setFormErrors("username", "Email connot contain spaces.");
      hasError = true;
    } else if (!isEmail(username)) {
      this.setFormErrors("username", "Email is invalid.");
      hasError = true;
    }

    if (password === "") {
      this.setFormErrors("password", "Password is required.");
      hasError = true;
    }

    return hasError;
  };

  _handleInputChange = (event: SyntheticInputEvent<HTMLInputElement>) => {
    this.setState({
      values: {
        ...this.state.values,
        [event.target.id]: event.target.value
      }
    });
  };

  setFormErrors = (field: string, message: string) => {
    this.setState(state => ({
      errors: {
        ...state.errors,
        [field]: message
      }
    }));
  };

  _handleSigninSubmit = () => {
    if (!this._validateForm()) {
      const { username, password } = this.state.values;
      this.props.onFormSubmit({
        username: username.trim(),
        password
      });
    }
  };

  _toggleRememberMe = () => {
    this.setState(prevState => ({
      rememberMe: !prevState.rememberMe
    }));
  };

  _loadFacebookSDK() {
    window.fbAsyncInit = function () {
      window.FB.init({
        appId: config.social.facebook,
        autoLogAppEvents: true,
        xfbml: true,
        version: "v3.1"
      });
    };

    (function (d, s, id) {
      var js,
        fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {
        return;
      }
      js = d.createElement(s);
      js.id = id;
      js.src = "https://connect.facebook.net/en_US/sdk.js";
      // $FlowFixMe
      fjs.parentNode.insertBefore(js, fjs);
    })(document, "script", "facebook-jssdk");
  }

  _getNotifications = () => {
    if (this.props.notifications !== null) {
      return this.props.notifications.message;
    }
    return null;
  };

  render() {
    {/*const Buttons = props => (
      <Fragment>
        <Button
          htmlType={Button.htmlType.BUTTON}
          type={Button.type.FACEBOOK}
          onClick={props.facebookSignIn}
        >
          <Icon library={Icon.FONTAWESOME_BRAND} icon="facebook-f" />
          LOGIN WITH FACEBOOK
        </Button>
        <Button
          htmlType={Button.htmlType.BUTTON}
          type={Button.type.GOOGLE}
          onClick={props.googleSignIn}
        >
          <Icon library={Icon.FONTAWESOME_BRAND} icon="google" />
          LOGIN WITH GOOGLE
        </Button>
      </Fragment>
    );

    const Federated = withFederated(Buttons);

    const federatedConfig = {
      google_client_id: config.social.google,
      facebook_app_id: config.social.facebook
    };*/}

    return (
      <Fragment>
        <form autoComplete="off">
          <div className="form-group">
            <label>Email</label>
            <Input
              id="username"
              placeholder="Eg: user@example.com"
              onChange={this._handleInputChange}
              error={this.state.errors.username}
              autoComplete={false}
            />
          </div>
          <div className="form-group">
            <label>Password</label>
            <Input
              id="password"
              placeholder="at least 6 characters including a number, a special character, lower case and upper case letters."
              type="password"
              onChange={this._handleInputChange}
              error={this.state.errors.password}
              autoComplete={false}
            />
            {this.props.isCapsLockActive && (
              <span className="warning">
                <Icon icon="exclamation-circle" /> Warning: Caps lock enabled.
              </span>
            )}
          </div>
          <Checkbox
            text="Register now"
            isChecked={false}
            onChange={() => this.props.toggleAuthType(AUTH_TYPE.SIGNUP)}
          />
          <div className="secondary-link">
            Forgot your password?{" "}
            <span
              onClick={() =>
                this.props.toggleAuthType(AUTH_TYPE.FORGOT_PASSWORD)
              }
              className="link"
            >
              Reset password.
            </span>
          </div>
        </form>
        {this._getNotifications()}
        <div className="button-container">
          <Button
            htmlType={Button.htmlType.BUTTON}
            onClick={() => this._handleSigninSubmit()}
            loading={this.props.isLoading}
          >
            LOGIN AS STUDENT
          </Button>
          {/*<div className="button-group">
            <Federated
              federated={federatedConfig}
              onStateChange={this.props.handleAuthStateChange}
            />
            </div>*/}
        </div>
      </Fragment>
    );
  }
}

export default SignIn;
