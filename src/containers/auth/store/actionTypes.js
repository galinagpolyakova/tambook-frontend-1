export const PREFIX = "@auth/";

export const AUTH_INIT = `${PREFIX}AUTH_INIT`;
export const AUTH_FAILURE = `${PREFIX}AUTH_FAILURE`;

export const AUTH_SIGNIN_SUCCESS = `${PREFIX}AUTH_SIGNIN_SUCCESS`;

export const AUTH_SIGNUP_SUCCESS = `${PREFIX}AUTH_SIGNUP_SUCCESS`;

export const IS_USER_AUTHENTICATED_SUCCUSS = `${PREFIX}IS_USER_AUTHENTICATED_SUCCUSS`;
export const AUTH_SIGNOUT_SUCCESS = `${PREFIX}AUTH_SIGNOUT_SUCCESS`;

export const IS_USER_AUTHENTICATED_FAILURE = `${PREFIX}IS_USER_AUTHENTICATED_FAILURE`;

export const CLEAR_NOTIFICATIONS = `${PREFIX}CLEAR_NOTIFICATIONS`;

export const AUTH_FORGOT_PASSWORD_SUCCESS = `${PREFIX}AUTH_FORGOT_PASSWORD_SUCCESS`;

export const AUTH_FORGOT_PASSWORD_SUBMIT_SUCCESS = `${PREFIX}AUTH_FORGOT_PASSWORD_SUBMIT_SUCCESS`;

export const AUTH_CONFIRM_SIGN_UP_SUCCESS = `${PREFIX}AUTH_CONFIRM_SIGN_UP_SUCCESS`;
