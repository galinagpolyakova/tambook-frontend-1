// @flow
import { type Action } from "../../../shared/types/ReducerAction";

import {
  SEND_CONTACT_MESSAGE_INIT,
  SEND_CONTACT_MESSAGE_SUCCESS,
  SEND_CONTACT_MESSAGE_FAILURE
} from "./actionTypes";

export type GeneralState = {
  isLoading: boolean,
  isContactMessageSendSuccess: boolean
};

const intialState: GeneralState = {
  isLoading: false,
  isContactMessageSendSuccess: false
};

function sendContactMessageInit(state) {
  return {
    ...state,
    isLoading: true,
    isContactMessageSendSuccess: false
  };
}

function sendContactMessageSuccess(state) {
  return {
    ...state,
    isLoading: false,
    isContactMessageSendSuccess: true,
  };
}

function sendContactMessageFailure(state) {
  return {
    ...state,
    isLoading: false,
    isContactMessageSendSuccess: false
  };
}

const reducer = (
  state: GeneralState = intialState,
  { type }: Action
) => {
  switch (type) {
    case SEND_CONTACT_MESSAGE_INIT:
      return sendContactMessageInit(state);
    case SEND_CONTACT_MESSAGE_SUCCESS:
      return sendContactMessageSuccess(state);
    case SEND_CONTACT_MESSAGE_FAILURE:
      return sendContactMessageFailure(state);
    default:
      return state;
  }
};

export default reducer;