import {
  SEND_CONTACT_MESSAGE_INIT,
  SEND_CONTACT_MESSAGE_SUCCESS,
  SEND_CONTACT_MESSAGE_FAILURE
} from "./actionTypes";

export function sendContactMessage(messageDetails) {
  return (dispatch, getState, serviceManager) => {
    dispatch(sendContactMessageInit());

    let generalService = serviceManager.get("GeneralService");
    generalService
      .sendContactMessage(messageDetails)
      .then(() => {
        dispatch(
          sendContactMessageSuccess()
        );
      }
      )
      .catch(err => {
        dispatch(sendContactMessageFailure(err));
      });
  };
}

function sendContactMessageInit() {
  return {
    type: SEND_CONTACT_MESSAGE_INIT
  };
}

function sendContactMessageSuccess() {
  return {
    type: SEND_CONTACT_MESSAGE_SUCCESS,
  };
}

function sendContactMessageFailure() {
  return {
    type: SEND_CONTACT_MESSAGE_FAILURE
  };
}