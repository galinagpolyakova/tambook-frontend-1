import React, { PureComponent } from "react";
import Section from "../../../../../shared/components/Section/Section";
import SectionHeader from "../../../../../shared/components/Section/SectionHeader";
import SectionBody from "../../../../../shared/components/Section/SectionBody";
import Row from "../../../../../shared/components/Row";
import Col from "../../../../../shared/components/Col";
import Button from "../../../../../shared/components/Button";
import CityCard from "../components/CityCard";

import australiaImage from "../../../../../assets/home/countries/australia.png";
import sydneyImage from "../../../../../assets/home/countries/australia/sydney.jpg";
import adelaideImage from "../../../../../assets/home/countries/australia/adelaide.jpg";
import byronBayImage from "../../../../../assets/home/countries/australia/byron-bay.jpg";
import noosaImage from "../../../../../assets/home/countries/australia/noosa.jpg";

import newZealandImage from "../../../../../assets/home/countries/New-Zealand.png";
import aucklandImage from "../../../../../assets/home/countries/new-zealand/auckland.jpg";
import queenstownImage from "../../../../../assets/home/countries/new-zealand/queenstown.jpg";
import rotoruaImage from "../../../../../assets/home/countries/new-zealand/rotorua.jpg";
import wellingtonImage from "../../../../../assets/home/countries/new-zealand/wellington.jpg";

class CountriesSection extends PureComponent {
  render() {
    return (
      <Section className="container most-popular-countries-section">
        <SectionHeader
          heading="MOST POPULAR COUNTRIES"
          subheading="Search for courses in our most popular countries."
        />
        <SectionBody>
          <Row className="country-row">
            <Col>
              <Row className="city-row">
                <Col>
                  <CityCard
                    image={sydneyImage}
                    name="SYDNEY"
                    price="150"
                    currencyType="USD"
                    link="/under-construction"
                  />
                </Col>
                <Col>
                  <CityCard
                    image={noosaImage}
                    name="NOOSA"
                    price="150"
                    currencyType="USD"
                    link="/under-construction"
                  />
                </Col>
              </Row>
              <Row className="city-row">
                <Col>
                  <CityCard
                    image={byronBayImage}
                    name="BYRON BAY"
                    price="150"
                    currencyType="USD"
                    link="/under-construction"
                  />
                </Col>
                <Col>
                  <CityCard
                    image={adelaideImage}
                    name="ADELAIDE"
                    price="150"
                    currencyType="USD"
                    link="/under-construction"
                  />
                </Col>
              </Row>
            </Col>
            <Col
              styles={{
                backgroundImage: `url(${australiaImage})`,
                backgroundSize: "60%",
                backgroundRepeat: "no-repeat",
                backgroundPosition: "center"
              }}
            >
              <p className="heading-3">AUSTRALIA</p>
              <p>
                Australia deserves its reputation as a hot, sunny, and safe
                country with a thriving beach culture. Australia has language
                schools in major cities of up to five million people, as well as
                in beautiful destination towns. Work visas are available to
                students while they’re studying, depending on the course.
              </p>
              <p className="heading-5">BEST CITIES</p>
              <Row>
                <Col>
                  <ul>
                    <li>Sydney</li>
                    <li>Melbourne</li>
                    <li>Brisbane</li>
                    <li>Perth</li>
                  </ul>
                </Col>
                <Col>
                  <ul>
                    <li>Adelaide</li>
                    <li>Gold Coast</li>
                    <li>Newcastle</li>
                    <li>Canberra</li>
                  </ul>
                </Col>
                <Col>
                  <ul>
                    <li>Sunshine Coast</li>
                    <li>Noosa</li>
                    <li>Albany</li>
                    <li>Broome</li>
                  </ul>
                </Col>
              </Row>
              <Button
                htmlType={Button.htmlType.LINK}
                link="/under-construction"
              >
                VIEW ALL PLACES
              </Button>
            </Col>
          </Row>
          <Row className="country-row">
            <Col
              styles={{
                backgroundImage: `url(${newZealandImage})`,
                backgroundSize: "40%",
                backgroundRepeat: "no-repeat",
                backgroundPosition: "center"
              }}
            >
              <p className="heading-3">NEW ZEALAND</p>
              <p>
                New Zealand is a friendly, safe country with a huge variety of
                climates and landscapes. You’ll find it easy to travel around
                New Zealand by car or bus, finding experiences and other
                travellers where ever you go. Language schools can be found in
                the major cities and stunning regional towns. Work visas are
                available to students while they’re studying, depending on the
                course.
              </p>
              <p className="heading-5">BEST CITIES</p>
              <Row>
                <Col>
                  <ul>
                    <li>Auckland</li>
                    <li>Wellington</li>
                    <li>Christchurch</li>
                    <li>Hamilton</li>
                  </ul>
                </Col>
                <Col>
                  <ul>
                    <li>Tauranga</li>
                    <li>Napier</li>
                    <li>Dunedin</li>
                    <li>Palmerston North</li>
                  </ul>
                </Col>
                <Col>
                  <ul>
                    <li>New Plymouth</li>
                    <li>Nelson</li>
                    <li>Rotorua</li>
                    <li>Queenstown</li>
                  </ul>
                </Col>
              </Row>
              <Button
                htmlType={Button.htmlType.LINK}
                link="/under-construction"
              >
                VIEW ALL PLACES
              </Button>
            </Col>
            <Col>
              <Row className="city-row">
                <Col>
                  <CityCard
                    image={aucklandImage}
                    name="AUCKLAND"
                    price="150"
                    currencyType="USD"
                    link="/under-construction"
                  />
                </Col>
                <Col>
                  <CityCard
                    image={wellingtonImage}
                    name="WELLINGTON"
                    price="150"
                    currencyType="USD"
                    link="/under-construction"
                  />
                </Col>
              </Row>
              <Row className="city-row">
                <Col>
                  <CityCard
                    image={queenstownImage}
                    name="QUEENSTOWN"
                    price="150"
                    currencyType="USD"
                    link="/under-construction"
                  />
                </Col>
                <Col>
                  <CityCard
                    image={rotoruaImage}
                    name="ROTORUA"
                    price="150"
                    currencyType="USD"
                    link="/under-construction"
                  />
                </Col>
              </Row>
            </Col>
          </Row>
        </SectionBody>
      </Section>
    );
  }
}

export default CountriesSection;
