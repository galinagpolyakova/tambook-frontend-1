// @flow
import React, { PureComponent } from "react";

import Row from "../../../../../shared/components/Row";
import Col from "../../../../../shared/components/Col";
import Button from "../../../../../shared/components/Button";
import Select from "../../../../../shared/components/Select";

import heroSliderImage from "../../../../../assets/home/hero-slider-image.jpg";

type MainSliderProps = {
  countries: any[],
  languages: any[]
};

type MainSliderState = {
  country: string,
  language: string
};

class MainSlider extends PureComponent<MainSliderProps, MainSliderState> {
  state = {
    country: "",
    language: ""
  };
  render() {
    const { countries, languages } = this.props;
    return (
      <div
        className="hero-slider"
        style={{
          background: `linear-gradient(
              rgba(0, 160, 186, 0.6),
              rgba(0, 160, 186, 0.6)
              )
              , center / cover no-repeat url(${heroSliderImage})`
        }}
      >
        <div className="container">
          <span className="hero-heading">
            Find, compare and book the best priced language courses.
          </span>
          <div className="search-box">
            <form action="/filter" method="get">
              <Row>
                <Col md="5">
                  <div className="form-group">
                    <label>Where do you want to learn?</label>
                    <input
                      type="hidden"
                      name="country"
                      value={this.state.country}
                    />
                    <Select
                      defaultSelected={true}
                      onChange={country => {
                        this.setState({ country });
                      }}
                      selected={this.state.country}
                      options={[
                        { name: "All Countries", value: "" },
                        ...countries.map(({ country }) => country)
                      ]}
                    />
                  </div>
                </Col>
                <Col md="5">
                  <div className="form-group">
                    <label>What language do you want to learn?</label>
                    <input
                      type="hidden"
                      name="language"
                      value={this.state.language}
                    />
                    <Select
                      defaultSelected={true}
                      onChange={language => {
                        this.setState({ language });
                      }}
                      selected={this.state.language}
                      options={[
                        { name: "All Languages", value: "" },
                        ...languages.map(({ language }) => language)
                      ]}
                    />
                  </div>
                </Col>
                <div className="form-group search-button">
                  <Button htmlType={Button.htmlType.SUBMIT}>SEARCH NOW</Button>
                </div>
              </Row>
            </form>
          </div>
          <span className="hero-subheading">
            Search hundreds of top rated, government approved language schools
            from around the world.
          </span>
        </div>
      </div>
    );
  }
}

export default MainSlider;
