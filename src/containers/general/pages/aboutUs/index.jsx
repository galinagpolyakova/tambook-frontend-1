import React, { PureComponent } from 'react';

import PageHeader from "../../../../components/PageHeader";

import DetailsSection from "./partials/DetailsSection";

import "./styles.css";

class AboutUs extends PureComponent {
  render() {
    return (
      <div>
        <PageHeader
          title="TAMBOOK - WE MAKE LEARNING LANGUAGES"
          subtitle="AN AFFORDABLE ADVENTURE"
          breadcrumbs={[
            {
              title: "ABOUT"
            }
          ]}
        />
        <DetailsSection />
      </div>
    );
  }
}

export default AboutUs;