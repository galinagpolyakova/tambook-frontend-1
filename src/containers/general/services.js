// @flow
import type { ApiServiceInterface } from "../../shared/services/ApiServiceInterface";

export class GeneralService {
  api: ApiServiceInterface;

  endpoint: string = "/schools/contact";

  constructor(apiService: ApiServiceInterface) {
    this.api = apiService;
  }

  async request(endpoint: string, query: Object = {}, options: Object = {}) {
    let response = await this.api.get(endpoint, query, options);

    return response;
  }

  sendContactMessage(payload: Object) {
    return this.api.post(this.endpoint, payload);
  }
}