import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import Amplify from "aws-amplify";

import registerServiceWorker from "./registerServiceWorker";
import { registerServices, serviceManager } from "./services/manager";
import { isUserAuthenticated } from "./containers/auth/store/action";

import awsConfig from "./config/amplify";
import initializeStore from "./store/config";
import Routes from "./routes";
import { SITE_URL } from "./config/app";

import "./index.css";
import { loadCoursesInCompare } from "./containers/course/store/actions";

Amplify.configure({
  Auth: awsConfig.cognito,
  Storage: awsConfig.storage
});

const settings = {
  api: {
    baseUrl: `${SITE_URL}/pre-prod`
  }
};

if (process.env.NODE_ENV === "development") {
  settings.api.baseUrl = "/api";
}
registerServices(settings);

const store = initializeStore({}, serviceManager);

store.dispatch(isUserAuthenticated());
store.dispatch(loadCoursesInCompare());
ReactDOM.render(
  <Provider store={store}>
    <Routes />
  </Provider>,
  document.getElementById("root")
);
registerServiceWorker();
