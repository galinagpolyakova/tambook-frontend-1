// @flow
import React, { Component, Fragment } from "react";
import { connect } from "react-redux";

type CurrencyProps = {
  userCurrency: string,
  currencyType: string,
  children: any,
  rates: Object
};

class Currency extends Component<CurrencyProps> {
  render() {
    const { userCurrency, currencyType, children, rates } = this.props;

    if (rates === null) {
      return null;
    }

    return (
      <Fragment>
        {userCurrency} {(children / rates[currencyType]).toFixed(2)}
      </Fragment>
    );
  }
}
function mapStateToProps(state) {
  return {
    userCurrency: state.profile.settings.currency,
    rates: state.profile.rates
  };
}
export default connect(mapStateToProps)(Currency);
