// @flow
import type { BreadcrumbListType } from "../../shared/components/Breadcrumb";

import React, { PureComponent } from "react";

import Breadcrumb from "../../shared/components/Breadcrumb";

import { isNotEmpty } from "../../shared/utils";

import auckland from "../../assets/auckland.jpg";

import "./styles.css";

type PageHeaderProps = {
  title: string,
  subtitle?: string,
  image: string,
  subheading?: string,
  breadcrumbs: BreadcrumbListType
};

class PageHeader extends PureComponent<PageHeaderProps> {
  static defaultProps = {
    image: auckland
  };
  render() {
    const { title, subtitle, image, subheading, breadcrumbs } = this.props;
    return (
      <div
        className="page-header"
        style={{
          background: `linear-gradient(
            #e8bc55b3, #e8bc55b3
            ), url(${image}) no-repeat right`,
          backgroundSize: "cover"
        }}
      >
        <div className="container">
          <Breadcrumb breadcrumbs={breadcrumbs} />
          <h1>{title}</h1>
          {isNotEmpty(subtitle) ? <h1 className="title2">{subtitle}</h1> : ""}
          {isNotEmpty(subheading) ? subheading : ""}
        </div>
        <div className="overlay" />
      </div>
    );
  }
}

export default PageHeader;
