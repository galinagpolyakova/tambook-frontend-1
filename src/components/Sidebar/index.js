import Sidebar from "./Sidebar";
import Widget from "./Widget";

import "./styles.css";

export default Sidebar;

export { Sidebar, Widget };
