// @flow
import React, { PureComponent } from "react";
import type { Element } from "react";

type SidebarProps = {
  children: | Array<Element<any> | string | typeof undefined>
    | Element<any>
    | string
    | typeof undefined
};

class Sidebar extends PureComponent<SidebarProps> {
  render() {
    return <div className="sidebar">{this.props.children}</div>;
  }
}

export default Sidebar;
