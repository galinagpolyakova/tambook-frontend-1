// @flow
import type { Node } from "react";
import React, { PureComponent } from "react";
import Header from "../Header";
import Footer from "../Footer";

type LayoutProps = {
  children?: Node,
  isAuthenticated: boolean,
  className: string
};

class Layout extends PureComponent<LayoutProps> {
  render() {
    const { children, isAuthenticated, className } = this.props;
    return (
      <div className={className}>
        <Header isAuthenticated={isAuthenticated} />
        {children}
        <Footer />
      </div>
    );
  }
}

export default Layout;
