import { composeWithDevTools } from "redux-devtools-extension";
import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import rootReducer from "./reducers";

export default function(intialState = {}, serviceManager) {
  const middlewares = [thunk.withExtraArgument(serviceManager)];

  const middlewareEnhancer = applyMiddleware(...middlewares);

  const composedEnhancers = composeWithDevTools(middlewareEnhancer);
  const store = createStore(rootReducer, intialState, composedEnhancers);

  return store;
}
