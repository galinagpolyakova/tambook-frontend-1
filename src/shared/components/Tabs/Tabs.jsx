// @flow
import React, { PureComponent } from "react";
import classnames from "classnames";

import Nav from "../Nav";
import NavItem from "../NavItem";
import NavLink from "../NavLink";
import TabPane from "./TabPane";

import "./styles.css";

export type TabType = {
  title: string,
  content: any
};

type TabsProps = {
  items: Array<TabType>
};

type TabsState = {
  activeTab: number
};

export default class Tabs extends PureComponent<TabsProps, TabsState> {
  state = {
    activeTab: 0
  };

  toggle = (tab: number) => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  };

  isActive = (index: number) => {
    return this.state.activeTab === index;
  };

  render() {
    const { items } = this.props;

    const navHeader = items.map((item, index) => {
      return (
        <NavItem key={index}>
          <NavLink
            className={classnames({
              active: this.isActive(index)
            })}
            onClick={() => {
              this.toggle(index);
            }}
          >
            {item.title}
          </NavLink>
        </NavItem>
      );
    });

    const navBody = items.map((item, index) => {
      return (
        <TabPane key={index} isActive={this.isActive(index)}>
          {item.content}
        </TabPane>
      );
    });

    return (
      <div className="tabs">
        <Nav tabs>{navHeader}</Nav>
        <div className="tab-content">{navBody}</div>
      </div>
    );
  }
}
