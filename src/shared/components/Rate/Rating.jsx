// @flow
import React, { PureComponent } from "react";
import classnames from "classnames";

import Rate from "./Rate";

import "./styles.css";

type RatingProps = {
  rate: number,
  reviews: number,
  size: | typeof Rate.size.SMALL
    | typeof Rate.size.MEDIUM
    | typeof Rate.size.LARGE
};
class Rating extends PureComponent<RatingProps> {
  static size = {
    SMALL: Rate.size.SMALL,
    MEDIUM: Rate.size.MEDIUM,
    LARGE: Rate.size.LARGE
  };
  static defaultProps = {
    size: Rating.size.MEDIUM
  };
  render() {
    const { rate, reviews, size } = this.props;
    return (
      <div className={classnames("ratings", size)}>
        <Rate value={rate} size={size} />
        <span>{reviews} Reviews</span>
      </div>
    );
  }
}

export default Rating;
