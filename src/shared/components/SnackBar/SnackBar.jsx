// @flow
import React, { Component } from "react";
import classNames from "classnames";

import "./styles.css";

type SnackBarProps = {
  show: boolean,
  timer: number,
  children: any
};

type SnackBarState = {
  showSnackBar: boolean
};

export default class SnackBar extends Component<SnackBarProps, SnackBarState> {
  static defaultProps = {
    show: "false",
    timer: 4000
  };

  constructor(props: SnackBarProps) {
    super(props);
    this.state = {
      showSnackBar: props.show
    };
  }

  componentWillReceiveProps(nextProps: SnackBarProps) {
    const { showSnackBar } = this.state;
    const { timer } = this.props;
    if (showSnackBar !== nextProps.show) {
      this.setState({
        showSnackBar: nextProps.show
      });

      setTimeout(() => {
        this.setState({ showSnackBar: false });
      }, timer);
    }
  }

  render() {
    const { showSnackBar } = this.state;

    return (
      <div
        className={classNames("snackbar", {
          active: showSnackBar
        })}
      >
        {this.props.children}
      </div>
    );
  }
}
