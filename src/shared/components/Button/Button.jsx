// @flow
import type { Element } from "react";

import React, { PureComponent } from "react";
import classNames from "classnames";
import { Link } from "react-router-dom";

import "./styles.css";

type ButtonProps = {
  type: | typeof Button.type.DEFAULT
    | typeof Button.type.PRIMARY
    | typeof Button.type.INFO
    | typeof Button.type.LIGHT
    | typeof Button.type.SECONDARY,
  size: | typeof Button.size.SMALL
    | typeof Button.size.MEDIUM
    | typeof Button.size.LARGE,
  outline?: boolean,
  fullWidth?: boolean,
  className?: string,
  link?: string,
  htmlType?: | typeof Button.htmlType.BUTTON
    | typeof Button.htmlType.SUBMIT
    | typeof Button.htmlType.LINK,
  children: | Array<Element<any> | string | typeof undefined>
    | Element<any>
    | string
    | typeof undefined,
  onClick?: Function,
  loading: boolean
};

class Button extends PureComponent<ButtonProps> {
  static size = {
    SMALL: "small",
    MEDIUM: "medium",
    DEFAULT: "default",
    LARGE: "large"
  };

  static htmlType = {
    BUTTON: "button",
    SUBMIT: "submit",
    LINK: "Link"
  };

  static type = {
    DEFAULT: "default",
    PRIMARY: "primary",
    INFO: "info",
    LIGHT: "light",
    SECONDARY: "secondary",
    FACEBOOK: "facebook",
    GOOGLE: "google"
  };

  static defaultProps = {
    type: Button.type.SECONDARY,
    size: Button.size.MEDIUM,
    outline: false,
    fullWidth: false,
    htmlType: Button.htmlType.BUTTON,
    loading: false
  };

  handleClick(e: SyntheticEvent<HTMLElement>) {
    if (this.props.onClick !== undefined) {
      this.props.onClick(e);
    }
  }

  getButtonContent = () => {
    return this.props.loading ? (
      <div className="loading">
        <div className="loader-spin" />
      </div>
    ) : (
      this.props.children
    );
  };

  render() {
    const {
      link,
      type,
      size,
      outline,
      className,
      fullWidth,
      htmlType,
      loading
    } = this.props;
    const buttonClasses = classNames(
      "btn",
      `btn-${type}`,
      `btn-${size}`,
      className,
      { "full-width": fullWidth },
      {
        "btn-outline": outline
      },
      {
        "is-loading": loading
      }
    );

    return htmlType === Button.htmlType.LINK && link !== undefined ? (
      <Link
        to={link}
        onClick={this.handleClick.bind(this)}
        className={buttonClasses}
      >
        {this.getButtonContent()}
      </Link>
    ) : (
      <button
        onClick={this.handleClick.bind(this)}
        type={htmlType}
        className={buttonClasses}
        disabled={loading}
      >
        {this.getButtonContent()}
      </button>
    );
  }
}

export default Button;
