// @flow
import React, { PureComponent } from "react";
import { Calendar } from "./Calendar";
import isSameDay from "../../utils/dates/isSameDay";
import addYears from "../../utils/dates/addYears";
import startOfMonth from "../../utils/dates/startOfMonth";
import addMonths from "../../utils/dates/addMonths";
import format from "../../utils/dates/format";

import { type RRuleSet, rrulestr } from "rrule";
import "./styles.css";
import Input from "../Input";
import Icon from "../Icon";

type DatepickerProps = {
  onToggleDate: (day: Date) => void,
  initialSelection: Date | null,
  dayFullTextFormat: string,
  monthFullTextFormat: string,
  minDate: Date,
  maxDate: Date,
  readonly: boolean,
  displaySelectedDates: "left" | "right" | "bottom" | "none",
  rrule?: RRuleSet,
  initialMonthToShow: Date,
  error: null | string
};

type DatepickerState = {
  displayedMonth: Date,
  selectedDate: Date | null,
  isOpen: boolean
};

const now = new Date();

export class Datepicker extends PureComponent<
  DatepickerProps,
  DatepickerState
  > {
  static defaultProps = {
    initialSelection: null,
    dayFullTextFormat: "yyyy-MM-dd",
    monthFullTextFormat: "MMM yyyy",
    minDate: addYears(now, -100),
    maxDate: addYears(now, 100),
    readonly: false,
    displaySelectedDates: "bottom",
    initialMonthToShow: new Date(),
    rrule: "",
    error: null
  };

  constructor(props: DatepickerProps) {
    super(props);

    this.state = {
      isOpen: false,
      displayedMonth: startOfMonth(this.props.initialMonthToShow),
      selectedDate: props.initialSelection
    };

    //$FlowFixMe
    this.onToggleDate = this.onToggleDate.bind(this);
    //$FlowFixMe
    this.handleMonthChange = this.handleMonthChange.bind(this);
  }

  getFieldErrors(error: string | null) {
    return error !== null ? (
      <ul className="form-errors">
        <li>{error}</li>
      </ul>
    ) : (
        ""
      );
  }

  getFormatedDate(date: Date) {
    return format(date, this.props.dayFullTextFormat);
  }

  onToggleDate(day: Date) {
    if (this.props.readonly) {
      return;
    }
    const selectedDate = this.getFormatedDate(day);
    if (this.isDayAlreadySelected(day)) {
      this.setState({
        selectedDate,
        isOpen: false
      });
    } else {
      this.setState({
        selectedDate,
        isOpen: false
      });
    }

    this.props.onToggleDate(selectedDate);
  }

  isDayAlreadySelected(day: Date) {
    return isSameDay(day, this.state.selectedDate);
  }

  handleMonthChange(direction: number) {
    this.setState({
      displayedMonth: addMonths(this.state.displayedMonth, direction)
    });
  }

  getPublicationCycle(): RRuleSet {
    if (this.props.rrule === "") {
      return null;
    }
    return rrulestr(this.props.rrule);
  }

  closeDatepicker = () => {
    this.setState({
      isOpen: false
    });
  };

  render() {
    const { error } = this.props;
    const hasErrors = error !== null;
    return (
      <div className="datepicker-container">
        <div className="datepicker-input-container">
          <div className="duration-icon">
            <Icon icon="calendar-alt" />
          </div>
          <Input
            className={`datepicker-input ${hasErrors ? 'has-error' : ''}`}
            text={this.state.selectedDate}
            onFocus={() =>
              this.setState({
                isOpen: true
              })
            }
          />
        </div>
        {this.getFieldErrors(error)}
        {this.state.isOpen && (
          <Calendar
            dayFullTextFormat={this.props.dayFullTextFormat}
            monthFullTextFormat={this.props.monthFullTextFormat}
            selectedDate={this.state.selectedDate}
            displaySelectedDates={this.props.displaySelectedDates}
            onDayClick={this.onToggleDate}
            displayedMonth={this.state.displayedMonth}
            minDate={this.props.minDate}
            maxDate={this.props.maxDate}
            readonly={this.props.readonly}
            onMonthChange={this.handleMonthChange}
            rrule={this.getPublicationCycle()}
            closeDatepicker={this.closeDatepicker}
          />
        )}
      </div>
    );
  }
}
