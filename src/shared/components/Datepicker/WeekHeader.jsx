// @flow
import React, { PureComponent } from "react";
import format from "../../utils/dates/format";
import setDay from "../../utils/dates/setDay";
import addDays from "../../utils/dates/addDays";
import eachDay from "../../utils/dates/eachDay";
import classNames from "classnames";

type WeekHeaderProps = {
  weekStartOn: "monday" | "sunday"
};

export class WeekHeader extends PureComponent<WeekHeaderProps> {
  getDays(): Date[] {
    let startDate = setDay(
      new Date(),
      this.props.weekStartOn === "monday" ? 1 : 0
    );
    let endDate = addDays(startDate, 6);
    return eachDay(startDate, endDate);
  }

  render() {
    return (
      <div className="week-header">
        {this.getDays().map((day, i) => {
          return (
            <span
              key={i}
              className={classNames("day", format(day, "EEEEEE").toLowerCase())}
            >
              {format(day, "EEEEEE")}
            </span>
          );
        })}
      </div>
    );
  }
}
