// @flow
import React, { PureComponent } from "react";
import "./styles.css";

type CheckboxProps = {
  isChecked: boolean,
  text: string,
  onChange: Function
};

type CheckboxState = {
  isChecked: boolean
};

class Checkbox extends PureComponent<CheckboxProps, CheckboxState> {
  static defaultProps = {
    isChecked: false,
    onChange: () => {}
  };

  state = {
    isChecked: false
  };

  toggleModal = (isChecked: boolean) => {
    this.setState(function() {
      return {
        isChecked
      };
    });
  };

  componentDidUpdate(prevProps: CheckboxProps) {
    prevProps.isChecked !== this.props.isChecked
      ? this.toggleModal(this.props.isChecked)
      : "";
  }

  render() {
    const { isChecked, text, onChange } = this.props;
    return (
      <div className="checkbox-container">
        <input type="checkbox" checked={isChecked} onChange={onChange} />
        <span className="checkmark" />
        <label>{text}</label>
      </div>
    );
  }
}

export default Checkbox;
