import Section from "./Section";
import SectionBody from "./SectionBody";
import SectionHeader from "./SectionHeader";

export default Section;
export { Section, SectionBody, SectionHeader };
