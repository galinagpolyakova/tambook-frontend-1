// @flow
import type { Element } from "react";
import React, { PureComponent } from "react";
import classNames from "classnames";

type SectionBodyProps = {
  children: | Array<Element<any> | string | typeof undefined>
    | Element<any>
    | string
    | typeof undefined,
  className: string
};

class SectionBody extends PureComponent<SectionBodyProps> {
  render() {
    const { children, className } = this.props;
    return (
      <div className={classNames("section-body", className)}>{children}</div>
    );
  }
}

export default SectionBody;
