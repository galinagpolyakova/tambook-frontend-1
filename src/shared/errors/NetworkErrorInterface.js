// @flow
export interface NetworkErrorInterface<Response> {

    constructor(response: Response): void;

    getResponse(): Response
}
