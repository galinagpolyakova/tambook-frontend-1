// @flow
import type { NetworkErrorInterface } from "./NetworkErrorInterface";

export default class ConnectionError extends Error implements NetworkErrorInterface<any> {
    _response: any;

    constructor(response: any) {
        super('Server unreachable');
        this._response = response;
    }

    getResponse(): any {
        return this._response;
    }
}
