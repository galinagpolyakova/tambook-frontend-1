// import formatDistance from './_lib/formatDistance'
// import formatLong from './_lib/formatLong'
// import formatRelative from './_lib/formatRelative'
// import localize from './_lib/localize'
// import match from './_lib/match'

/**
 * @type {Locale}
 * @category Locales
 * @summary Portuguese locale.
 * @language Portuguese
 * @iso-639-2 por
 * @author Dário Freire [@dfreire]{@link https://github.com/dfreire}
 */
// var locale = {
//   formatDistance: formatDistance,
//   formatLong: formatLong,
//   formatRelative: formatRelative,
//   localize: localize,
//   match: match,
//   options: {
//     weekStartsOn: 1 /* Monday */,
//     firstWeekContainsDate: 4
//   }
// }

// export default locale

throw new Error(
  "pt locale is currently unavailable. Please check the progress of converting this locale to v2.0.0 in this issue on Github: TBA"
);
