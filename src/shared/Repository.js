// @flow
import type { SerializableInterface } from "./SerializableInterface";

export class Repository<T: SerializableInterface<any>, U> {
  _store: Array<T> = [];

  _comparator(a: T, b: T): boolean {
    return a === b;
  }

  add(item: T) {
    const i = this._store.findIndex((element: T) => {
      return this._comparator(item, element);
    });
    if (i !== -1) {
      return;
    }

    this._store.push(item);
  }

  toJSON(): Array<U> {
    return this._store.map((element: T) => {
      return element.toJSON();
    });
  }
}
